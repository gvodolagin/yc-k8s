variable "folder-id" {
  description = "folder id"
  default     = ""
}

variable "cloud-id" {
  description = "cloud id"
  default     = ""
}

variable "token" {
  description = "token"
  default     = ""
}

variable "access-key" {
  description = "access-key"
  default     = ""
}

variable "secret-key" {
  description = "secret-key"
  default     = ""
}
