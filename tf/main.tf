terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"

  backend "s3" {
    endpoint   = "storage.yandexcloud.net"
    bucket     = "terraform-state-gvodolagin"
    region     = "ru-central1"
    key        = "k8s/terraform.tfstate"
    access_key = ""
    secret_key = ""

    skip_region_validation      = true
    skip_credentials_validation = true
  }
}

provider "yandex" {
  token     = var.token
  cloud_id = var.cloud-id
  folder_id = var.folder-id
}

# Network

resource "yandex_vpc_network" "default" {
  name = "default"
  description   = "Managed by Terraform"
}

resource "yandex_vpc_subnet" "subnet-a" {
  v4_cidr_blocks = ["10.128.0.0/24"]
  zone           = "ru-central1-a"
  network_id     = "${yandex_vpc_network.default.id}"
  description   = "Managed by Terraform"
}

resource "yandex_vpc_subnet" "subnet-b" {
  v4_cidr_blocks = ["10.129.0.0/24"]
  zone           = "ru-central1-b"
  network_id     = "${yandex_vpc_network.default.id}"
  description   = "Managed by Terraform"
}

resource "yandex_vpc_subnet" "subnet-c" {
  v4_cidr_blocks = ["10.130.0.0/24"]
  zone           = "ru-central1-c"
  network_id     = "${yandex_vpc_network.default.id}"
  description   = "Managed by Terraform"
}

# Service accounts

resource "yandex_iam_service_account" "this" {
  folder_id = "${var.folder-id}"
  name = "k8s-sa"
}

resource "yandex_resourcemanager_folder_iam_binding" "editor" {
  folder_id = "${var.folder-id}"
  role = "editor"
  members = [
    "serviceAccount:${yandex_iam_service_account.this.id}"
  ]
  depends_on = [yandex_iam_service_account.this]
}

resource "yandex_iam_service_account" "ingress" {
  folder_id = "${var.folder-id}"
  name = "ingress-sa"
}

resource "yandex_resourcemanager_folder_iam_binding" "alb_editor" {
  folder_id = "${var.folder-id}"
  role = "alb.editor"
  members = [
    "serviceAccount:${yandex_iam_service_account.ingress.id}"
  ]
  depends_on = [yandex_iam_service_account.ingress]
}

resource "yandex_resourcemanager_folder_iam_binding" "vpc_publicAdmin" {
  folder_id = "${var.folder-id}"
  role = "vpc.publicAdmin"
  members = [
    "serviceAccount:${yandex_iam_service_account.ingress.id}"
  ]
  depends_on = [yandex_iam_service_account.ingress]
}

resource "yandex_resourcemanager_folder_iam_binding" "certificate-manager_certificates_downloader" {
  folder_id = "${var.folder-id}"
  role = "certificate-manager.certificates.downloader"
  members = [
    "serviceAccount:${yandex_iam_service_account.ingress.id}"
  ]
  depends_on = [yandex_iam_service_account.ingress]
}

resource "yandex_resourcemanager_folder_iam_binding" "compute_viewer" {
  folder_id = "${var.folder-id}"
  role = "compute.viewer"
  members = [
    "serviceAccount:${yandex_iam_service_account.ingress.id}"
  ]
  depends_on = [yandex_iam_service_account.ingress]
}

# Security groups

resource "yandex_vpc_security_group" "k8s-main-sg" {
  name        = "k8s-main-sg"
  description = "Правила группы обеспечивают базовую работоспособность кластера."
  network_id  = "${yandex_vpc_network.default.id}"

  ingress {
    protocol       = "TCP"
    description    = "Правило разрешает проверки доступности с диапазона адресов балансировщика нагрузки. Нужно для работы отказоустойчивого кластера и сервисов балансировщика."
    v4_cidr_blocks = ["10.128.0.0/24","10.129.0.0/24","10.130.0.0/24"]
    from_port      = 0
    to_port        = 65535
  }
  ingress {
    protocol          = "ANY"
    description       = "Правило разрешает взаимодействие мастер-узел и узел-узел внутри группы безопасности."
    predefined_target = "self_security_group"
    from_port         = 0
    to_port           = 65535
  }
  ingress {
    protocol       = "ANY"
    description    = "Правило разрешает взаимодействие под-под и сервис-сервис."
    v4_cidr_blocks = ["10.128.0.0/24","10.129.0.0/24","10.130.0.0/24"]
    from_port      = 0
    to_port        = 65535
  }
    ingress {
    protocol       = "TCP"
    description    = "Правило разрешает входящий трафик для kubectl."
    v4_cidr_blocks = ["0.0.0.0/0"]
    from_port      = 443
    to_port        = 443
  }
  ingress {
    protocol       = "ICMP"
    description    = "Правило разрешает отладочные ICMP-пакеты из внутренних подсетей."
    v4_cidr_blocks = ["10.128.0.0/24","10.129.0.0/24","10.130.0.0/24"]
  }
  egress {
    protocol       = "ANY"
    description    = "Правило разрешает весь исходящий трафик. Узлы могут связаться с Yandex Container Registry, Object Storage, Docker Hub и т. д."
    v4_cidr_blocks = ["0.0.0.0/0"]
    from_port      = 0
    to_port        = 65535
  }
}

resource "yandex_vpc_security_group" "k8s-public-services" {
  name        = "k8s-public-services"
  description = "Правила группы разрешают подключение к сервисам из интернета."
  network_id  = "${yandex_vpc_network.default.id}"

  ingress {
    protocol       = "TCP"
    description    = "Правило разрешает входящий трафик из интернета на диапазон портов NodePort."
    v4_cidr_blocks = ["0.0.0.0/0"]
    from_port      = 30000
    to_port        = 32767
  }
}

# K8S


resource "yandex_kubernetes_cluster" "zonal_cluster_resource_name" {
  name        = "k8s-gvodolagin"
  description = "Managed by Terraform"

  network_id = "${yandex_vpc_network.default.id}"

  master {
    version = "1.20"
    zonal {
      zone      = "${yandex_vpc_subnet.subnet-a.zone}"
      subnet_id = "${yandex_vpc_subnet.subnet-a.id}"
    }

    public_ip = true

    security_group_ids = [yandex_vpc_security_group.k8s-main-sg.id]

    maintenance_policy {
      auto_upgrade = false

    }
  }

  service_account_id      = yandex_iam_service_account.this.id
  node_service_account_id = yandex_iam_service_account.this.id

  labels = {
    env       = "dev"
  }

  release_channel = "STABLE"
  network_policy_provider = "CALICO"

}

resource "yandex_kubernetes_node_group" "my_node_group" {
  cluster_id  = "${yandex_kubernetes_cluster.zonal_cluster_resource_name.id}"
  name        = "node-a"
  description = "Managed by Terraform"
  version     = "1.20"

  labels = {
    "env" = "dev"
  }

  instance_template {
    platform_id = "standard-v2"

    network_interface {
      nat                = true
      subnet_ids         = ["${yandex_vpc_subnet.subnet-a.id}"]

      security_group_ids = [
        yandex_vpc_security_group.k8s-main-sg.id,
        yandex_vpc_security_group.k8s-public-services.id
      ]
    }

    resources {
      memory = 4
      cores  = 2
      core_fraction = 5
    }

    boot_disk {
      type = "network-hdd"
      size = 64
    }

    scheduling_policy {
      preemptible = false
    }

    container_runtime {
      type = "containerd"
    }
  }

  scale_policy {
    fixed_scale {
      size = 1
    }
  }

  allocation_policy {
    location {
      zone = "ru-central1-a"
    }
  }

  maintenance_policy {
    auto_upgrade = false
    auto_repair  = false

    # maintenance_window {
    #   day        = "monday"
    #   start_time = "15:00"
    #   duration   = "3h"
    # }

    # maintenance_window {
    #   day        = "friday"
    #   start_time = "10:00"
    #   duration   = "4h30m"
    # }
  }
}

