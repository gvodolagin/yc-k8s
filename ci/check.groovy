pipeline {
    agent any

    parameters {
        string(name: 'CHECK_URL', defaultValue: 'https://yandex.ru', description: 'healt check', trim: true)
        booleanParam(name: 'NOTIFY', defaultValue: true, description: 'send slack notify')
    }

    environment{
        CMD = "curl --write-out %{http_code} --silent --output /dev/null ${CHECK_URL}"

    }

    stages {
        stage('Stage-One') {
            steps {
                script{
                    sh "${CMD} > commandResult"
                    env.status = readFile('commandResult').trim()
                }
            }
        }
        stage('Stage-Two') {
            steps {
                script {
                    sh "echo ${env.status}"
                    if (env.status == '200') {
                        currentBuild.result = "SUCCESS"
                    }
                    else {
                        currentBuild.result = "FAILURE"
                    }
                }
            }
        }
    }
    post {
        always {
            emailext (
                subject: "Job '${env.JOB_NAME} [${env.BUILD_NUMBER}] ${currentBuild.result}'",
                to: "azsfartv@yandex.ru;",
                body: """<p>STARTED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]':</p>
                    <p>Check console output at &QUOT;<a href='${env.BUILD_URL}'>${env.JOB_NAME} [${env.BUILD_NUMBER}]</a>&QUOT;</p>""",
                )
        }
    }
}